# Integração e Entrega / Implantação Contínuas


## 

- [x] A Integração Contínua (CI) é um processo que envolve a incorporação regular e automatizada de alterações em um repositório de código compartilhado.
- [X] Implantação e/ou Entrega Contínua (CD) é um processo bifásico que envolve a integração, teste e distribuição de alterações de código.
- [X] A entrega contínua é quase como uma implantação automática em um ambiente de produção, enquanto a implantação contínua envolve o lançamento automático de atualizações no ambiente de produção.

Para melhor conhecer os termos leia: [O que é CI/CD?](https://www.redhat.com/pt-br/topics/devops/what-is-ci-cd)

***

# Projeto Final: Implementação do Módulo de Integração e Entrega Contínuas (CI/CD)

Foi criado uma [pipeline](https://docs.gitlab.com/ee/ci/pipelines/) para uma aplicação web. Uma pipeline é um conjunto de processos automatizados que permitem aos desenvolvedores validar e entregar novas alterações de código de forma mais eficiente e confiável. A pipeline que foi criada consiste em dois componentes principais: Integração Contínua (CI) e Entrega Contínua (CD).

A parte de Integração Contínua (CI) da pipeline se concentra em garantir a qualidade do código. Isso é feito através da integração frequente de alterações de código na branch principal. O processo de CI inclui os seguintes passos:

- **Build**: Este passo envolve a instalação das dependências necessárias para a aplicação.
```
build:
  stage: build
  image: maven:3.8.7-openjdk-18-slim
  script:
    - mvn install
```
- **Testes**: Este passo envolve a execução de vários testes para garantir a qualidade do código. Isso inclui testes unitários, que verificam o funcionamento individual de partes específicas do código, e testes de estilo de código, que garantem que o código segue as diretrizes de estilo de codificação estabelecidas.
```
unit test:
  stage: test
  image: maven:3.8.7-openjdk-18-slim
  script:
    - mvn test

code style:
  stage: test
  image: maven:3.8.7-openjdk-18-slim
  script:
    - mvn checkstyle::check

coverage:
  stage: coverage
  image: maven:3.8.7-openjdk-18-slim
  script:
    - mvn jacoco:report
```
A parte de Entrega Contínua (CD) da pipeline se concentra em garantir que a nova versão candidata do software esteja funcionando corretamente. O processo de CD inclui os seguintes passos:

- **Certificação de que a versão candidata está saudável**: Este passo envolve a verificação de que a nova versão da aplicação está funcionando corretamente.
```
health_check:
  stage: health_check
  script:
    - echo "Verificando a saúde da aplicação..."
    - HEALTH_CHECK_RESULT=$(curl --write-out '%{http_code}' --silent --output /dev/null http://ec2-18-231-118-156.sa-east-1.compute.amazonaws.com:8080/healthcheck)
    - if [ $HEALTH_CHECK_RESULT -ne 200 ]; then echo "A aplicação não está saudável. Executando rollback..."; bash rollback.sh; else echo "A aplicação está saudável."; fi

```

- **Deploy da aplicação**: Este passo envolve a implantação da nova versão da aplicação.
```
deploy:
  variables:
    SERVER_IP: ec2-18-231-118-156.sa-east-1.compute.amazonaws.com
  stage: deploy
  script:
    # Configuração do SSH
    - echo "Implantando a aplicação..."
    - mkdir -p ~/.ssh
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_rsa
    - chmod 400 ~/.ssh/id_rsa

    # Configurando o host remoto
    - ssh -o StrictHostKeyChecking=no -i ~/.ssh/id_rsa ubuntu@ec2-18-231-118-156.sa-east-1.compute.amazonaws.com "rm -r /home/ubuntu/app; mkdir /home/ubuntu/app"

    # Copiando a aplicação para o servidor
    - scp -r -i ~/.ssh/id_rsa $(pwd)/src $(pwd)/pom.xml $(pwd)/Dockerfile $(pwd)/deploy.sh ubuntu@ec2-18-231-118-156.sa-east-1.compute.amazonaws.com:/home/ubuntu/app

    # Executando o script de deploy
    - ssh -o StrictHostKeyChecking=no -i ~/.ssh/id_rsa ubuntu@ec2-18-231-118-156.sa-east-1.compute.amazonaws.com  "cd /home/ubuntu/app ; bash deploy.sh"
```
A pipeline foi configurada para ser executada na plataforma [GitLab CI](https://about.gitlab.com).


## Ambiente de Deploy
O deploy da pipeline no GitLab foi realizado em uma instância [EC2](https://sa-east-1.console.aws.amazon.com/console/home?region=sa-east-1) da AWS. A instância EC2, identificada pelo endereço *ubuntu@ec2-18-231-118-156.sa-east-1.compute.amazonaws.com*, é um servidor virtual na nuvem da Amazon Web Services (AWS).

Neste caso, o ambiente de produção é a instância EC2 na AWS. O código é transferido para este servidor e então é executado lá, tornando as alterações do código disponíveis para os usuários finais.

## Testando a Aplicação 

- O endpoint raiz (“/”): Quando um usuário acessa a URL base da aplicação http://ec2-18-231-118-156.sa-east-1.compute.amazonaws.com:8080, o servidor responde com a mensagem “Hello World\n”. Isso é feito pelo método index().
  
````agsl
    ⚠️ Possivelmente, ao utilizar a URL acima, a instância na AWS pode não estar em execução.
````
- O endpoint de verificação de saúde (“/healthcheck”): Este é um endpoint comum em aplicações web para verificar se a aplicação está funcionando corretamente. Quando um usuário acessa http://ec2-18-231-118-156.sa-east-1.compute.amazonaws.com:8080/healthcheck, o servidor responde com um status HTTP 200, que significa “OK”. Isso é feito pelo método healthcheck().
```
Esse endpoint foi usado para a certificação de que a versão candidata está saudável.
```
